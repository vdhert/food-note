from django.urls import reverse
from django.views.generic import DetailView
from django.views.generic import ListView
from django.views.generic.edit import CreateView
from django.views.generic.edit import UpdateView

from meals4day.models import Day
from meals4day.models import Ingredient
from meals4day.models import Meal
from meals4day.models import QuantifiedIngredient


class DayListView(ListView):
    model = Day


class DayDetailView(DetailView):
    model = Day


class DayEditView(UpdateView):
    model = Day
    fields = "__all__"

    def get_success_url(self):
        self_pk = self.get_context_data()["object"].pk
        return reverse("day-detail", kwargs={"pk": self_pk})


class MealCreateView(CreateView):
    model = Meal
    fields = "__all__"

    def get_parent_day_pk(self):
        context = self.get_context_data()
        return context["view"].kwargs["daypk"]

    def get_success_url(self):
        return reverse("day-detail", kwargs={"pk": self.get_parent_day_pk()})

    def form_valid(self, form):
        url = super().form_valid(form)
        context = self.get_context_data()
        parent_day = Day.objects.get(pk=self.get_parent_day_pk())
        parent_day.meals.add(context["meal"])
        return url


class IngredientCreateView(CreateView):
    model = Ingredient
    fields = "__all__"


class QuantifiedIngredientCreateView(CreateView):
    model = QuantifiedIngredient
    fields = "__all__"
