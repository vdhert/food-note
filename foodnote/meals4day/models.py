from django.db import models


class Unit(models.Model):
    short = models.CharField(max_length=4)

    def __str__(self):
        return f"[{self.short}]"


class Ingredient(models.Model):
    name = models.CharField(max_length=32, unique=True)
    category = models.ForeignKey(
        "shopping.Category",
        null=True,
        blank=True,
        default=None,
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name


class QuantifiedIngredient(models.Model):
    ingredient = models.ForeignKey(Ingredient, on_delete=models.CASCADE)
    quantity = models.IntegerField()
    unit = models.ForeignKey(Unit, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.ingredient} {self.quantity} {self.unit}"


class Meal(models.Model):
    name = models.CharField(max_length=32, default="", blank=True)
    ingredients = models.ManyToManyField(QuantifiedIngredient)
    recipe = models.TextField(max_length=1024, default="", blank=True)

    def __str__(self):
        return self.name or f"Meal {self.pk}"


class Day(models.Model):
    name = models.CharField(max_length=128)
    meals = models.ManyToManyField(Meal)

    def __str__(self):
        return f"{self.pk} {self.name} ({len(self.meals.all())} meal/s)"
