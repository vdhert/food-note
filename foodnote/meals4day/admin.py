from django.contrib import admin

from meals4day.models import Day
from meals4day.models import Ingredient
from meals4day.models import Meal
from meals4day.models import QuantifiedIngredient
from meals4day.models import Unit

admin.site.register(Day)
admin.site.register(Ingredient)
admin.site.register(Meal)
admin.site.register(QuantifiedIngredient)
admin.site.register(Unit)
