from django.contrib import admin
from django.urls import path
from django.urls import re_path

from meals4day.views import DayDetailView
from meals4day.views import DayEditView
from meals4day.views import DayListView
from meals4day.views import IngredientCreateView
from meals4day.views import MealCreateView
from meals4day.views import QuantifiedIngredientCreateView
from shopping.views import ShoppingCycleListView
from shopping.views import ShoppingCycleView

urlpatterns = [
    path("foodnote/admin/", admin.site.urls),
    path("days/", DayListView.as_view(), name="day-list"),
    path("days/<int:pk>", DayDetailView.as_view(), name="day-detail"),
    path("days/edit/<int:pk>", DayEditView.as_view(), name="day-edit"),
    path(
        "meals/create-for-day-<int:daypk>", MealCreateView.as_view(), name="meal-create"
    ),
    path("ingredients/create", IngredientCreateView.as_view(), name="ing-create"),
    path(
        "ingredients/quantify",
        QuantifiedIngredientCreateView.as_view(),
        name="qing-create",
    ),
    path("shopping-list/", ShoppingCycleListView.as_view(), name="shopping-cycle-list"),
    path(
        "shopping-list/<int:pk>",
        ShoppingCycleView.as_view(),
        name="shopping-cycle-detail",
    ),
]
