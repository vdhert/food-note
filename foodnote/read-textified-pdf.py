import os
import re
import sys
from argparse import ArgumentParser
from pathlib import Path

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "foodnote.settings")

import django

django.setup()

from meals4day.models import Day
from meals4day.models import Ingredient
from meals4day.models import Meal
from meals4day.models import QuantifiedIngredient
from meals4day.models import Unit
from shopping.models import ShoppingCycle
from shopping.models import ShoppingDay

PERIODS = (
    ("Poniedziałek", "Wtorek"),
    ("Wtorek", "Środa"),
    ("Środa", "Czwartek"),
    ("Czwartek", "Piątek"),
    ("Piątek", "Sobota"),
    ("Sobota", "Niedziala"),
    ("Niedziela", "Lista zakupów"),
)

MEALS = ("ŚNIADANIE", "DRUGIE ŚNIADANIE", "OBIAD", "PODWIECZOREK", "II OBIAD")


def get_parser():
    parser = ArgumentParser()
    parser.add_argument("file", type=Path, nargs="*")
    parser.add_argument("--day-prefix", default="TYDZ1-")
    return parser


def main(args):
    for file in args.file:
        content = read_file(file)
        db_days = []
        for i in extract_days(content):
            dname = args.day_prefix + extract_day_name(i)
            meals = []
            for meal in extract_meals(i):
                name = extract_name(meal)
                recipe = extract_recipe(meal)
                ings = extract_ings(meal)
                db_ings = add_ings(ings)
                db_meal = add_meal(name, db_ings, recipe)
                meals.append(db_meal)
            db_days.append(add_day(dname, meals))
        add_cycle(db_days)
    return 0


def read_file(path):
    with open(path) as fh:
        return fh.read()


def extract_days(content):
    day_chunks = []
    for start, end in PERIODS:
        start = content.find(start)
        end = content.find(end)
        day_chunks.append(content[start:end])
    return day_chunks


def extract_day_name(day):
    return re.match(r"(\w*)", day).group(1).lower()


def extract_meals(day):
    meals = []
    for start, end in zip(MEALS, MEALS[1:] + (None,)):
        start_ind = day.find(start)
        end_ind = None
        if end is not None:
            end_ind = day.find(end)
        text = day[start_ind:end_ind]
        meals.append(text)
    return meals


def extract_name(meal):
    line2 = meal.split("\n")[2]
    return line2[: line2.find("1. ")].strip().lower()


def extract_recipe(meal):
    lines = meal.split("\n")[2:]
    start_ind = lines[0].find("1. ")
    return "\n".join([line[start_ind:] for line in lines]).strip()


def extract_ings(meal):
    lines = meal.split("\n")[2:]
    items = []
    for line in lines:
        match = re.match(r"(?P<name>.*?) - (?P<q>[0-9]*) (?P<u>[a-z]+)", line)
        if not match:
            continue
        item = (match.group("name").lower(), match.group("q"), match.group("u"))
        items.append(item)
    return items


def add_ings(ings):
    db_objs = []
    for name, quantity, unit in ings:
        unit_obj, _ = Unit.objects.get_or_create(short=unit)
        ingredient_obj, _ = Ingredient.objects.get_or_create(name=name)
        qing, _ = QuantifiedIngredient.objects.get_or_create(
            ingredient=ingredient_obj, quantity=int(quantity), unit=unit_obj
        )
        db_objs.append(qing)
    return db_objs


def add_meal(name, qings, recipe):
    meal_obj, _ = Meal.objects.get_or_create(name=name, recipe=recipe)
    for qing in qings:
        meal_obj.ingredients.add(qing)
    return meal_obj


def add_day(name, meals):
    day_obj, _ = Day.objects.get_or_create(name=name)
    for meal in meals:
        day_obj.meals.add(meal)
    return day_obj


def add_cycle(days):
    cycle, _ = ShoppingCycle.objects.get_or_create()
    for day in days:
        sday, _ = ShoppingDay.objects.get_or_create(day=day, repeatitions=1)
        cycle.days.add(sday)


if __name__ == "__main__":
    args = get_parser().parse_args()
    sys.exit(main(args))
