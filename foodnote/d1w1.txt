 Poniedziałek                                                                                                                                          Tymoteusz Oleniecki

 ŚNIADANIE 09:30                                                                              K:599.6 / B:29.6 / T:28.5 / WP:50.6 / F:7.9 / WW:1.3

 KANAPKA CAPRESE Z PESTO                                                                      1. Pieczywo posmaruj pesto.
 Chleb orkiszowy - 120 g (4 x Kromka)                                                         2. Pokrój mozzarellę i pomidora w plasterki.
 Ser, mozzarella - 60 g (4 x Porcja)                                                          3. Ułóż ser i pomidora na przemian.
 Pomidor - 120 g (1 x Sztuka)                                                                 4. Posyp pestkami dyni.
 Pesto zielone z bazylii - 20 g (1 x Łyżka)                                                   5. Opcjonalnie możesz włożyć kanapki do pieca na około 5-10 minut, będą jeszcze
 Dynia, pestki, łuskane - 10 g (1 x Łyżka)                                                    smaczniejsze.
                                                                                              6. Opakowania mozzarelli ważą zwykle 120g - zużywamy zatem 1/2 opakowania.

 DRUGIE ŚNIADANIE 11:30                                                                       K:434.7 / B:31.1 / T:14.4 / WP:39.3 / F:5.3 / WW:3.9

 SEREK WIEJSKI Z NASIONAMI                                                                    1. Ogórki drobno pokrój. Nasiona upraż na suchej patelni (opcjonalnie).
 Dynia, pestki, łuskane - 10 g (1 x Łyżka)                                                    2. Wszystko wymieszaj z serkiem i zjedz z bułką.
 Ogórek - 80 g (2 x Sztuka)
 Bułki grahamki - 65 g (1 x Sztuka)
 Serek wiejski (naturalny) - 200 g (1 x Opakowanie)

OBIAD 13:00                                                          K:754.6 / B:43.5 / T:30.9 / WP:79.6 / F:9.8 / WW:8.0

CURRY Z TOFU (PRZEPIS NA 2 PORCJE)                                   1. Ugotuj ryż.
Zjedz 1 z 2 porcji                                                   2. Pokrój cukinię, paprykę i tofu w kostkę. Podsmaż warzywa.
Oliwa z oliwek - 25 g (2.5 x Łyżka)                                  3. Wymieszaj sos sojowy z cytryną, 1/2 podanej ilości oliwy, z przyprawami. Obtocz tofu w
Papryka słodka (mielona, wędzona) - 2 g (0.4 x Łyżeczka)             marynacie i przechowaj w lodówce przez ok. 1 godzinę (opcjonalnie).
Papryka czerwona - 140 g (1 x Sztuka)                                4. Podsmaż tofu.
Ryż jaśminowy - 150 g (10 x Łyżka)                                   5. Dodaj na patelnię warzywa. Duś całość do miękkości, doprawiając do smaku.
Cukinia - 300 g (1 x Sztuka)                                         Wymieszaj z jogurtem, wyłącz gotowanie.
Mielona papryka chili - 2 g (2 x Szczypta)                           6. Podaj z ryżem i posyp posiekaną pietruszką.
Pietruszka, liście - 12 g (2 x Łyżeczka)                             7. Możesz zmniejszyć ilość warzyw, jeśli będzie zbyt dużo objętościowo.
Jogurt naturalny, 2% tłuszczu - 120 g (6 x Łyżka)
Czosnek granulowany - 2 g (2 x Szczypta)
Imbir mielony - 2 g (0.4 x Łyżeczka)
Sok cytrynowy - 12 g (2 x Łyżka)
Sos sojowy jasny - 20 g (2 x Łyżka)
Tofu naturalne (fortyfikowane Ca) - 360 g (2 x Opakowanie)
Curry - 2 g (2 x Szczypta)

PODWIECZOREK 17:00                                                   K:559.8 / B:20.2 / T:19.9 / WP:77.1 / F:7.4 / WW:7.7

JOGURT Z GRANOLĄ I BANANEM                                           1. Wymieszaj jogurt z granolą.
Banan - 120 g (1 x Sztuka)                                           2. Dodaj obranego i pokrojonego banana.
Jogurt naturalny, 2% tłuszczu - 250 g (12.5 x Łyżka)
Granola - 60 g (6 x Łyżka)

II OBIAD 20:30                                                       K:754.6 / B:43.5 / T:30.9 / WP:79.6 / F:9.8 / WW:8.0

CURRY Z TOFU (PRZEPIS NA 2 PORCJE)
Zjedz 1 z 2 porcji
Potrawa powinna być już gotowa.
Przepis znajdziesz powyżej.

                                                                     SUMA K: 3103.2 B: 167.9 T: 124.6 WP: 326.2 F: 40.2 WW: 28.8



Wtorek                                                                                                                       Tymoteusz Oleniecki

ŚNIADANIE 09:30                                                      K:591.7 / B:21.0 / T:22.3 / WP:79.4 / F:8.3 / WW:7.9

JOGURT Z GRANOLĄ I WINOGRONAMI                                       1. Wymieszaj jogurt z granolą.
Jogurt naturalny, 2% tłuszczu - 250 g (12.5 x Łyżka)                 2. Dodaj wypestkowane (polecam kupić od razu takie) i pokrojone winogrona.
Granola - 70 g (7 x Łyżka)                                           3. Możesz dodać odrobinę kakao do smaku.
Winogrona - 140 g (2 x Garść)

DRUGIE ŚNIADANIE 11:30                                               K:624.6 / B:17.1 / T:25.5 / WP:75.1 / F:13.6 / WW:7.4

KANAPKI Z HUMMUSEM                                                   1. Bułkę przekrój na pół i posmaruj hummusem.
Hummus z suszonymi pomidorami - 80 g (8 x Łyżeczka)                  2. Zjedz z ogórkami.
Ogórek - 120 g (3 x Sztuka)
Bułki grahamki - 130 g (2 x Sztuka)
OBIAD 13:00                                                          K:687.0 / B:45.6 / T:20.4 / WP:72.7 / F:8.9 / WW:7.2

INDYK W MAŚLE ORZECHOWYM Z BROKUŁAMI (PRZEPIS NA 2 PORCJE)           1. Mięso pokrój w kostkę (zamiennie tofu/ seitan/ tempeh/ krewetki lub dorsz w tej samej
Zjedz 1 z 2 porcji                                                   ilości).
Cebula czerwona - 100 g (1 x Sztuka)                                 2. Zamarynuj w sosie z łyżki oliwy, sosu sojowego, masła orzechowego, soku z limonki,
Papryczka ostra (chili) - 20 g (1 x Sztuka)                          rozgniecionego czosnku i przypraw.
Czosnek - 5 g (1 x Ząbek)                                            3. Na patelni na pozostałej oliwie zeszklij cebulkę. Dodaj pokrojoną drobno papryczkę chili
Sos sojowy jasny - 20 g (2 x Łyżka)                                  (ilość wg uznania).
Oliwa z oliwek - 20 g (2 x Łyżka)                                    4. Dodaj na patelnię mięso i smaż do miękkości.
Masło orzechowe - 30 g (2 x Łyżeczka)                                5. Zjedz z ugotowanymi brokułami i ryżem (polecam używać sypkiego, nie w woreczkach).
Imbir mielony - 2.5 g (0.5 x Łyżeczka)                               6. Można wymieszać wszystkie składniki razem przygotowując danie jednogarnkowe.
Ryż basmati - 150 g (10 x Łyżka)
Pieprz czarny mielony - 2 g (2 x Szczypta)
Brokuły, mrożone - 450 g (1 x Opakowanie)
Mięso z piersi indyka, bez skóry - 300 g (3 x Porcja)
Limonka - 29 g (0.5 x Sztuka)

PODWIECZOREK 17:00                                                   K:515.3 / B:28.9 / T:22.2 / WP:48.3 / F:3.7 / WW:4.9

SEREK WIEJSKI Z BANANEM                                              1. Obierz i i pokrój banana.
Orzechy nerkowca (bez soli) - 30 g (2 x Łyżka)                       2. Wymieszaj z serkiem.
Żurawina suszona - 12 g (1 x Łyżka)                                  3. Dodaj posiekane nerkowce i żurawinę.
Serek wiejski (naturalny) - 200 g (1 x Opakowanie)                   4. Możesz dodać odrobinę kakao lub cynamon do smaku.
Banan - 120 g (1 x Sztuka)

II OBIAD 20:30                                                       K:687.0 / B:45.6 / T:20.4 / WP:72.7 / F:8.9 / WW:7.2

INDYK W MAŚLE ORZECHOWYM Z BROKUŁAMI (PRZEPIS NA 2 PORCJE)
Zjedz 1 z 2 porcji
Potrawa powinna być już gotowa.
Przepis znajdziesz powyżej.

                                                                     SUMA K: 3105.7 B: 158.1 T: 110.9 WP: 348.2 F: 43.4 WW: 34.5



Środa                                                                                                                         Tymoteusz Oleniecki

ŚNIADANIE 09:30                                                      K:703.9 / B:17.1 / T:25.8 / WP:95.6 / F:12.2 / WW:9.7

SHAKE SNICKERS                                                       1. Banany obierz i pokrój. Daktyle posiekaj.
Płatki owsiane - 30 g (3 x Łyżka)                                    2. Zmiksuj składniki.
Masło orzechowe - 30 g (2 x Łyżeczka)
Daktyle, suszone - 20 g (4 x Sztuka)
Mleko migdałowe naturalne bio - 250 g (1 x Szklanka)
Banan - 240 g (2 x Sztuka)
Kakao 16%, proszek - 10 g (1 x Łyżka)

DRUGIE ŚNIADANIE 11:30                                               K:510.0 / B:30.8 / T:20.7 / WP:42.9 / F:8.3 / WW:4.3

SEREK Z AWOKADO I ZIOŁAMI                                            1. Pomidorki przekrój na pół.
Bułki grahamki - 65 g (1 x Sztuka)                                   2. Awokado przekrój na pół, pozbaw pestki, pokrój .
Serek wiejski (naturalny) - 200 g (1 x Opakowanie)                   3. Wszystko wymieszaj z serkiem, dopraw.
Awokado - 70 g (0.5 x Sztuka)                                        4. Zjedz z bułką.
Pomidory koktajlowe - 100 g (5 x Sztuka)
Sok cytrynowy - 6 g (1 x Łyżka)
Bazylia (suszona) - 1 g (0.25 x Łyżeczka)
Czosnek granulowany - 1 g (1 x Szczypta)
OBIAD 13:00                                                                   K:666.8 / B:39.6 / T:23.1 / WP:70.7 / F:12.2 / WW:7.2

INDYK Z PIECZONYMI WARZYWAMI (PRZEPIS NA 2 PORCJE)                            1. Pierś z indyka lekko rozbij, dopraw, na środku ułóż pokrojone pomidory i cebulę.
Zjedz 1 z 2 porcji                                                            2. Ziemniaki umyj, obierz i pokrój w paski. Paprykę pokrój w paski. Skrop warzywa oliwą i
Cebula czerwona - 100 g (1 x Sztuka)                                          dopraw.
Mięso z piersi indyka, bez skóry - 300 g (3 x Porcja)                         3. Wszystko razem piecz w piekarniku, warzywa będą gotowe po około 20 minutach,
Suszone pomidory (w oleju z ziołami, odsączone) - 90 g (6 x Sztuka)           mięso po 30.
Papryka czerwona - 140 g (1 x Sztuka)
Oliwa z oliwek - 30 g (3 x Łyżka)
Ziemniaki - 700 g (10 x Sztuka)

PODWIECZOREK 17:00                                                            K:555.4 / B:27.5 / T:25.2 / WP:49.4 / F:8.0 / WW:1.2

TOSTY PEŁNOZIARNISTE Z SEREM MOZZARELLA                                       1. Pieczywo zrumień w opiekaczu (opcjonalnie). Posmaruj masłem.
Masło ekstra - 10 g (2 x Łyżeczka)                                            2. Na kromkach ułóż rukolę, plastry sera i pokrojonego pomidora.
Chleb orkiszowy - 120 g (4 x Kromka)                                          3. Posyp do smaku oregano.
Rukola - 40 g (2 x Garść)                                                     4. Opakowania mozzarelli ważą zwykle 120g - zużywamy zatem 1/2 opakowania.
Ser, mozzarella - 60 g (4 x Porcja)
Oregano (suszone) - 1 g (0.33 x Łyżeczka)
Pomidor - 120 g (1 x Sztuka)

II OBIAD 20:30                                                                K:666.8 / B:39.6 / T:23.1 / WP:70.7 / F:12.2 / WW:7.2

INDYK Z PIECZONYMI WARZYWAMI (PRZEPIS NA 2 PORCJE)
Zjedz 1 z 2 porcji
Potrawa powinna być już gotowa.
Przepis znajdziesz powyżej.

                                                                              SUMA K: 3103.0 B: 154.5 T: 117.8 WP: 329.3 F: 53.0 WW: 29.6



Czwartek                                                                                                                                Tymoteusz Oleniecki

ŚNIADANIE 09:30                                                               K:609.6 / B:37.0 / T:24.5 / WP:53.7 / F:9.7 / WW:1.7

GRZANKI Z AWOKADO I TWAROŻKIEM                                                1. Posiekaj szczypiorek i wymieszaj z twarogiem oraz ulubionymi przyprawami.
Chleb orkiszowy - 120 g (4 x Kromka)                                          2. Posmaruj chleb awokado, nałóż twarożek.
Twaróg półtłusty - 125 g (0.5 x Opakowanie)                                   3. Złóż kromki i przekrój na trójkąty. Zapiecz w opiekaczu/ na patelni (opcjonalnie).
Szczypiorek - 10 g (2 x Łyżeczka)                                             4. Podaj z pomidorkami.
Pomidory koktajlowe - 120 g (6 x Sztuka)
Awokado - 75 g (0.54 x Sztuka)

DRUGIE ŚNIADANIE 11:30                                                        K:552.0 / B:31.0 / T:24.1 / WP:46.6 / F:6.5 / WW:4.8

SHAKE PROTEINOWY                                                              1. Banana obierz i pokrój. Daktyle posiekaj.
Serek wiejski (naturalny) - 200 g (1 x Opakowanie)                            2. Zmiksuj składniki.
Mleko migdałowe naturalne bio - 250 g (1 x Szklanka)
Masło orzechowe - 15 g (1 x Łyżeczka)
Daktyle, suszone - 10 g (2 x Sztuka)
Banan - 120 g (1 x Sztuka)
Kakao 16%, proszek - 10 g (1 x Łyżka)
OBIAD 13:00                                                                   K:624.8 / B:29.1 / T:21.0 / WP:72.9 / F:20.0 / WW:7.3

PENNE A'LA BOLOGNA (PRZEPIS NA 2 PORCJE)                                      1. Ciecierzycę odlej z zalewy i opłucz (220-240g to zwykle 1 puszeczka - można użyć
Zjedz 1 z 2 porcji                                                            ugotowanej w tej samej ilości).
Ser, mozzarella - 60 g (4 x Porcja)                                           2. Cebulkę zeszklij na oliwie.
Oregano (suszone) - 2 g (0.67 x Łyżeczka)                                     3. Dodaj pomidory i rozgnieciony czosnek.
Bazylia (suszona) - 2 g (0.5 x Łyżeczka)                                      4. Dopraw sos ziołami i przyprawami.
Oliwa z oliwek - 20 g (2 x Łyżka)                                             5. Wymieszaj sos z cieciorką.
Ciecierzyca (w zalewie) - 240 g (12 x Łyżka)                                  6. Makaron ugotuj al' dente.
Czosnek - 10 g (2 x Ząbek)                                                    7. Wymieszaj wszystko łącznie.
Cebula - 100 g (1 x Sztuka)                                                   8. Posyp startym serem (60g to zwykle 1/2 opakowania).
Pomidory z puszki (krojone) - 400 g (4 x Porcja)
Makaron penne (pełnoziarnisty) - 140 g (2 x Szklanka)

PODWIECZOREK 17:00                                                            K:691.4 / B:19.6 / T:34.4 / WP:73.2 / F:19.8 / WW:7.3

WRAPY Z HUMMUSEM I SUSZONYMI POMIDORAMI (PRZEPIS NA 2                         1. Przygotuj wypełnienie: rukolę umyj i osusz, pomidory i paprykę pokrój w paski, plastry
PORCJE)                                                                       cebuli przekrój na pół.
Zjedz 1 z 2 porcji                                                            2. Placek zwilż wodą i połóż na suchą, rozgrzaną patelnię. Ogrzewaj przez 10 sekund z
Tortilla pełnoziarnista - 244 g (4 x Sztuka)                                  każdej strony.
Suszone pomidory (w oleju z ziołami, odsączone) - 60 g (4 x Sztuka)           3. Zdejmij placek z patelni. Posmaruj hummusem, a następnie ułóż na środku wszystkie
Rukola - 80 g (4 x Garść)                                                     składniki.
Cebula - 100 g (1 x Sztuka)                                                   4. Zwiń placek – możesz przekłuć wykałaczkami, by wrapy się nie rozpadły.
Hummus z suszonymi pomidorami - 120 g (12 x Łyżeczka)
Papryka czerwona - 140 g (1 x Sztuka)

II OBIAD 20:30                                                                K:624.8 / B:29.1 / T:21.0 / WP:72.9 / F:20.0 / WW:7.3

PENNE A'LA BOLOGNA (PRZEPIS NA 2 PORCJE)
Zjedz 1 z 2 porcji
Potrawa powinna być już gotowa.
Przepis znajdziesz powyżej.

                                                                              SUMA K: 3102.6 B: 145.9 T: 125.1 WP: 319.3 F: 75.9 WW: 28.3



Piątek                                                                                                                                 Tymoteusz Oleniecki

ŚNIADANIE 09:30                                                               K:599.7 / B:22.5 / T:24.7 / WP:74.9 / F:9.6 / WW:7.4

JOGURT Z GRANOLĄ I MANDARYNKAMI                                               1. Wymieszaj jogurt z granolą.
Jogurt naturalny, 2% tłuszczu - 250 g (12.5 x Łyżka)                          2. Dodaj obrane i podzielone na cząstki mandarynki.
Granola - 80 g (8 x Łyżka)                                                    3. Możesz dodać odrobinę kakao do smaku.
Mandarynki - 130 g (2 x Sztuka)

DRUGIE ŚNIADANIE 11:30                                                        K:517.6 / B:13.3 / T:22.9 / WP:61.2 / F:9.7 / WW:3.3

KANAPKI Z AWOKADO                                                             1. Na chleb nałóż pokrojone składniki.
Chleb orkiszowy - 90 g (3 x Kromka)                                           2. Z awokado możesz również przygotować pastę: pokrojone zmiksować na gładką masę z
Pomidor - 120 g (1 x Sztuka)                                                  dodatkiem ulubionych przypraw, rozgniecionym czosnkiem i sokiem z cytryny.
Awokado - 70 g (0.5 x Sztuka)

BAKALIE                                                                       1. Dodatkowo zjedz bakalie.
Żurawina suszona - 24 g (2 x Łyżka)
Orzechy nerkowca (bez soli) - 15 g (1 x Łyżka)
OBIAD 13:00                                                       K:648.4 / B:46.6 / T:19.2 / WP:64.9 / F:13.4 / WW:6.4

DORSZ Z ZIELONYM PESTO, WARZYWAMI I RYŻEM (PRZEPIS NA 2           1. Dorsza posmaruj pesto, zawiń w folię aluminiową i upiecz w piekarniku (około 30 minut
PORCJE)                                                           w 180 stopniach)
Zjedz 1 z 2 porcji                                                2. Warzywa ugotuj lub przygotuj na beztłuszczowej patelni.
Warzywa na patelnię zielone - 450 g (4.5 x Porcja)                3. Ryż ugotuj.
Pesto zielone z bazylii - 90 g (4.5 x Łyżka)                      4. Rybę zjedz z warzywami i ryżem.
Dorsz, świeży, filety bez skóry - 400 g (4 x Porcja)              5. Udekoruj pomidorkami.
Ryż brązowy - 150 g (10 x Łyżka)
Pomidory koktajlowe - 120 g (6 x Sztuka)

PODWIECZOREK 17:00                                                K:691.4 / B:19.6 / T:34.4 / WP:73.2 / F:19.8 / WW:7.3

WRAPY Z HUMMUSEM I SUSZONYMI POMIDORAMI (PRZEPIS NA 2
PORCJE)
Zjedz 1 z 2 porcji
Potrawa powinna być już gotowa.
Przepis znajdziesz powyżej.

II OBIAD 20:30                                                    K:648.4 / B:46.6 / T:19.2 / WP:64.9 / F:13.4 / WW:6.4

DORSZ Z ZIELONYM PESTO, WARZYWAMI I RYŻEM (PRZEPIS NA 2
PORCJE)
Zjedz 1 z 2 porcji
Potrawa powinna być już gotowa.
Przepis znajdziesz powyżej.

                                                                  SUMA K: 3105.6 B: 148.5 T: 120.4 WP: 339.0 F: 65.7 WW: 30.9



Sobota                                                                                                                    Tymoteusz Oleniecki

ŚNIADANIE 09:30                                                   K:612.9 / B:37.3 / T:34.6 / WP:35.5 / F:8.2 / WW:1.7

DIABELSKIE JAJKA SADZONE                                          1. Posiekaj drobno papryczkę chilli. Podana ilość jest orientacyjna, użyj wg własnego
Cebula - 50 g (0.5 x Sztuka)                                      gustu.
Chleb orkiszowy - 60 g (2 x Kromka)                               2. Przeciśnij przez praskę czosnek. Cebulkę posiekaj.
Jaja kurze całe - 112 g (2 x Sztuka)                              3. Podsmaż na oliwie czosnek i cebulkę, dodaj po chwili papryczkę.
Pomidory z puszki (krojone) - 200 g (2 x Porcja)                  4. Wlej na patelnię pomidory z puszki i wbij jajka. Usmaż.
Czosnek - 5 g (1 x Ząbek)                                         5. Dopraw pieprzem.
Ser, mozzarella - 60 g (4 x Porcja)                               6. Zetrzyj ser mozzarella i posyp posiekanym szczypiorkiem. Zjedz z pieczywem.
Szczypiorek - 10 g (2 x Łyżeczka)                                 7. Opakowania sera mozzarella ważą zwykle 120g - zużywamy zatem 1/2 opakowania.
Papryczka ostra (chili) - 20 g (1 x Sztuka)
Oliwa z oliwek - 10 g (1 x Łyżka)

DRUGIE ŚNIADANIE 11:30                                            K:511.7 / B:31.0 / T:21.0 / WP:42.1 / F:8.8 / WW:4.2

AWOKADO ZE SZCZYPIORKIEM I CHILI                                  1. Awokado obierz ze skórki, wydrąż pestkę i pokrój drobno.
Bułki grahamki - 65 g (1 x Sztuka)                                2. Dodaj do serka wraz ze szczypiorkiem i chili.
Serek wiejski (naturalny) - 200 g (1 x Opakowanie)                3. Wymieszaj i dopraw dodatkowo wg uznania.
Awokado - 70 g (0.5 x Sztuka)                                     4. Zjedz z bułką.
Szczypiorek - 10 g (2 x Łyżeczka)                                 5. Udekoruj pomidorkami.
Mielona papryka chili - 2 g (2 x Szczypta)
Pomidory koktajlowe - 100 g (5 x Sztuka)
OBIAD 13:00                                                          K:560.4 / B:22.4 / T:13.6 / WP:79.5 / F:11.2 / WW:7.9

KOTLETY Z SOCZEWICY                                                  1. Ugotuj soczewicę i kaszę jaglaną.
Kasza jaglana - 52 g (4 x Łyżka)                                     2. Posiekaj drobno cebulę.
Soczewica czerwona, nasiona suche - 48 g (4 x Łyżka)                 3. Zmiksuj składniki razem z ulubionymi przyprawami, np. pieprzem, tymiankiem.
Cebula - 50 g (0.5 x Sztuka)                                         4. Uformuj kotleciki i posmaruj małą ilością oliwy.
Mąka pszenna (typ 2000, pełnoziarnista) - 15 g (1 x Łyżka)           5. Piecz w nagrzanym piekarniku (25 minut, 170 stopni).
Oliwa z oliwek - 5 g (0.5 x Łyżka)                                   6. Zjedz z surówką.

SURÓWKA Z BURAKÓW (PRZEPIS NA 2 PORCJE)                              1. Ugotuj buraki na parze lub tradycyjnie i zetrzyj na drobnych oczkach.
Zjedz 1 z 2 porcji                                                   2. Cebulę pokrój bardzo drobno i wymieszaj z burakiem i oliwą.
Cebula czerwona - 100 g (1 x Sztuka)                                 3. Dopraw do smaku pieprzem.
Oliwa z oliwek - 10 g (1 x Łyżka)                                    4. Możesz użyć już ugotowanych, dostępnych w sklepach buraczków, aby ułatwić
Burak - 200 g (2 x Sztuka)                                           przygotowanie dania.

PODWIECZOREK 17:00                                                   K:352.5 / B:7.2 / T:11.5 / WP:52.1 / F:6.2 / WW:5.4

KOKTAJL CZEKOLADOWY                                                  1. Owoc obierz i pokrój. Czekoladę rozdrobnij.
Mleko migdałowe naturalne bio - 250 g (1 x Szklanka)                 2. Zmiksuj składniki w blenderze.
Banan - 120 g (1 x Sztuka)                                           3. Zamiast czekolady możesz użyć niepełnej łyżki kakao.
Płatki owsiane - 25 g (2.5 x Łyżka)
Czekolada gorzka - 12 g (2 x Kostka)

II OBIAD 20:30                                                       K:1065.6 / B:42.2 / T:52.1 / WP:97.6 / F:9.5 / WW:2.0

ŁOSOŚ GRILLOWANY                                                     1. Skrop sokiem z cytryny rybę i posyp sezamem.
Sok cytrynowy - 12 g (2 x Łyżka)                                     2. Grilluj na patelni z rusztem po około 3 - 4 minuty z każdej strony lub przygotuj w
Łosoś, świeży - 150 g (1.5 x Porcja)                                 piekarniku (180 stopni, 25 minut).
Sezam, nasiona - 15 g (1.5 x Łyżka)                                  3. Zjedz z surówką.
Pieprz czarny mielony - 1 g (1 x Szczypta)

SURÓWKA Z BURAKÓW (PRZEPIS NA 2 PORCJE)
Zjedz 1 z 2 porcji
Potrawa powinna być już gotowa.
Przepis znajdziesz powyżej.

CHIPSY Z PIECA                                                       1. Tego dnia możesz zjeść paczuszkę swoich ulubionych chipsów (smak wg uznania).
Chipsy z pieca Lay's - 125 g (1 x Opakowanie)

                                                                     SUMA K: 3103.1 B: 140.2 T: 132.8 WP: 306.9 F: 43.9 WW: 21.2



Niedziela                                                                                                                      Tymoteusz Oleniecki

ŚNIADANIE 09:30                                                      K:717.9 / B:43.9 / T:29.5 / WP:72.0 / F:7.0 / WW:7.2

OWSIANE PLACKI Z BIAŁYM SEREM                                        1. Twaróg rozgnieć.
Dżem 100% owoców (różne rodzaje) - 30 g (2 x Łyżeczka)               2. Zmiksuj płatki, twaróg i białka.
Twaróg półtłusty - 125 g (0.5 x Opakowanie)                          3. Smaż na rozgrzanym oleju (zamiennie rzepakowy lub oliwa, aby nie kupować
Płatki owsiane - 80 g (8 x Łyżka)                                    dodatkowo).
Masło orzechowe - 15 g (1 x Łyżeczka)                                4. Gotowe posmaruj masłem orzechowym i dżemem o ulubionym smaku (zamiennie
Białko jaja kurzego - 70 g (2 x Sztuka)                              garstka sezonowych owoców).
Olej kokosowy (stały) - 10 g (0.5 x Łyżka)

DRUGIE ŚNIADANIE 11:30                                               K:547.0 / B:18.8 / T:26.6 / WP:53.1 / F:11.2 / WW:1.6

SEZAMOWE GRZANKI Z WARZYWAMI                                         1. Z chleba zrób grzanki (w tosterze/opiekaczu/na patelni) - opcjonalnie.
Ogórek - 80 g (2 x Sztuka)                                           2. Na grzanki nałóż tahini i pokrojone warzywa (1 grzanka z pomidorkami, druga grzanka z
Chleb orkiszowy - 120 g (4 x Kromka)                                 ogórkiem).
Pomidory koktajlowe - 160 g (8 x Sztuka)
Tahini - 36 g (6 x Łyżeczka)
OBIAD 13:00                                                    K:649.6 / B:32.8 / T:25.1 / WP:72.1 / F:8.0 / WW:7.2

MAKARON SOJOWY Z TOFU (PRZEPIS NA 2 PORCJE)                    1. Makaron przygotuj wg instrukcji.
Zjedz 1 z 2 porcji                                             2. Natłuść lekko patelnię i ułóż na niej pokrojoną cukinię i pieczarki (jeśli są małe, to
Sezam, nasiona - 10 g (1 x Łyżka)                              możemy smażyć w całości, dobrym wyborem będą również grzyby shitake). Smaż na dość
Rukola - 40 g (2 x Garść)                                      dużym ogniu, po 3 minuty z każdej strony.
Marchew - 90 g (2 x Sztuka)                                    3. Marchewkę pokrój obieraczką we wstążki.
Szczypiorek - 10 g (2 x Łyżeczka)                              4. Tofu pokrój w kostkę. Na woku/ patelni rozgrzej oliwę i dodaj tofu. Smaż, od czasu do
Cukinia - 300 g (1 x Sztuka)                                   czasu mieszając, aż będzie lekko rumiane.
Pieczarka uprawna, świeża - 120 g (6 x Sztuka)                 5. Wok/ patelnię przetrzyj, dodaj sos sojowy, wędzone tofu, szczypiorek i wymieszaj.
Pomidory koktajlowe - 120 g (6 x Sztuka)                       6. Miski napełnij makaronem, dodaj rukolę, pomidorki koktajlowe, marchew, pieczarki,
Tofu wędzone - 180 g (1 x Opakowanie)                          cukinię. Całość posyp tofu i sezamem.
Oliwa z oliwek - 20 g (2 x Łyżka)                              7. Możesz zmniejszyć ilość warzyw, jeśli będzie zbyt dużo objętościowo.
Makaron sojowy - 180 g (3 x Porcja)
Sos sojowy jasny - 25 g (2.5 x Łyżka)

PODWIECZOREK 17:00                                             K:537.4 / B:20.8 / T:20.0 / WP:69.8 / F:9.1 / WW:6.8

JOGURT Z GRANOLĄ I POMARAŃCZĄ                                  1. Wymieszaj jogurt z granolą.
Jogurt naturalny, 2% tłuszczu - 250 g (12.5 x Łyżka)           2. Dodaj obraną i podzieloną na cząstki pomarańczę.
Granola - 60 g (6 x Łyżka)                                     3. Możesz dodać odrobinę kakao do smaku.
Pomarańcza - 200 g (1 x Sztuka)

II OBIAD 20:30                                                 K:649.6 / B:32.8 / T:25.1 / WP:72.1 / F:8.0 / WW:7.2

MAKARON SOJOWY Z TOFU (PRZEPIS NA 2 PORCJE)
Zjedz 1 z 2 porcji
Potrawa powinna być już gotowa.
Przepis znajdziesz powyżej.

                                                               SUMA K: 3101.6 B: 149.1 T: 126.2 WP: 339.0 F: 43.3 WW: 30.0
