from django.contrib import admin

from shopping.models import Category
from shopping.models import ShoppingCycle
from shopping.models import ShoppingDay

admin.site.register(ShoppingDay)
admin.site.register(ShoppingCycle)
admin.site.register(Category)
