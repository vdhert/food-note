from collections import defaultdict
from itertools import groupby


class ShoppingListFactory:
    def from_shopping_cycle(self, cycle):
        slist = ShoppingList()
        for sday in cycle.days.all():
            multipicator = sday.repeatitions
            day = sday.day
            for meal in day.meals.all():
                for qingredient in meal.ingredients.all():
                    ingredient = qingredient.ingredient
                    unit = qingredient.unit
                    quantity = qingredient.quantity * multipicator
                    slist.add_item(ingredient, unit, quantity)
        return slist


class ShoppingList:
    def __init__(self):
        self.ingredients = defaultdict(int)

    def add_item(self, ingredient, unit, quantity):
        self.ingredients[(ingredient, unit)] += quantity

    def groupped_by_category(self):
        def get_category(key):
            ingredient, _ = key
            if ingredient.category is None:
                return ""
            return ingredient.category.name

        sorted_keys = sorted(self.ingredients, key=get_category)
        dct = {}
        for category, key_iterator in groupby(sorted_keys, key=get_category):
            dct[category] = tuple((*key, self.ingredients[key]) for key in key_iterator)
        return dct
