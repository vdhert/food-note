from django.db import models

from meals4day.models import Day
from shopping.list import ShoppingListFactory


class Category(models.Model):
    name = models.CharField(max_length=128, unique=True)

    def __str__(self):
        return self.name


class ShoppingDay(models.Model):
    day = models.ForeignKey(Day, on_delete=models.CASCADE)
    repeatitions = models.IntegerField(default=1)

    def __str__(self):
        return f"Day {self.day.name} x {self.repeatitions}"


class ShoppingCycle(models.Model):
    days = models.ManyToManyField(ShoppingDay)

    def __str__(self):
        days = ", ".join(str(i) for i in self.days.all())
        return f"Cycle: {days}"

    def get_shopping_list(self):
        return ShoppingListFactory().from_shopping_cycle(self)
