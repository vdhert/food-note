from django.shortcuts import render
from django.views.generic import DetailView
from django.views.generic import ListView

from shopping.models import ShoppingCycle


class ShoppingCycleListView(ListView):
    model = ShoppingCycle


class ShoppingCycleView(DetailView):
    model = ShoppingCycle
